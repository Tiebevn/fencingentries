package domain;


public class Fencer {

	String name;
	int birthdate;
	char sex;
	char handed;
	Club club;
	int license;
	Nation nationality;
	
	public Fencer(String name, int birthdate, char sex, char handed, Club club, int license, Nation nationality) {
		setName(name);
		setBirthdate(birthdate);
		setSex(sex);
		setHanded(handed);
		setClub(club);
		setLicense(license);
		setNationality(nationality);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (null == name || "".equals(name.trim())) throw new IllegalArgumentException();
		this.name = name;
	}

	public int getBirthdate() {
		
		return birthdate;
	}

	public void setBirthdate(int birthdate) {
		
		if (birthdate < 0) throw new IllegalArgumentException();
		
		this.birthdate = birthdate;
	}
	
	private boolean isValidSex(char sex) {
		return 'm' == sex  || 'f' == sex;
	}

	public char getSex() {
		
		
		return sex;
	}

	public void setSex(char sex) {
		
		if (!isValidSex(sex)) throw new IllegalArgumentException();
		
		this.sex = sex;
	}

	public char getHanded() {
		return handed;
	}

	private boolean isValidHand(char handed) {
		return 'r' == handed || 'l' == handed;
	}
	
	public void setHanded(char handed) {
		
		if (!isValidHand(handed)) throw new IllegalArgumentException();
		
		this.handed = handed;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		if(null == club) throw new IllegalArgumentException();
		
		this.club = club;
	}

	public int getLicense() {
		return license;
	}

	public void setLicense(int license) {
		if(license < 1) throw new IllegalArgumentException();
		this.license = license;
	}

	public Nation getNationality() {
		return nationality;
	}

	public void setNationality(Nation nationality) {
		if (null == nationality) throw new IllegalArgumentException();
		this.nationality = nationality;
	}
	
	
}
