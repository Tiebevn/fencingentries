package domain;

public class Nation {

	String name;
	String code;
	
	public Nation(String name, String code) {
		setName(name);
		setCode(code);
	}
	
	public Nation(String name) {
		this(name, null);
	}
	
	public void setName(String name) {
		
		if (name == null || name.trim().equals("")) throw new IllegalArgumentException();
		
		this.name = name;
	}
	
	private void setCode(String code) {
		
		if (null == code || "".equals(code.trim())) {
			if (null == this.name || "".equals(this.name.trim())) {
				throw new IllegalArgumentException();
			}
			this.code = this.name.substring(0, 2);
		}
		
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}
	
	
	
}
