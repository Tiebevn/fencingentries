package domain;

import java.util.ArrayList;



public class Event {

	int Date;
	int minAge;
	int maxAge;
	boolean womenAllowed;
	boolean menAllowed;
	boolean isTeamsEvent;
	int entryFee;
	String type;
	String formule;
	ArrayList<Fencer> Entries;
	
	public Event(int Date, int minAge, int maxAge, boolean womenAllowed, boolean menAllowed, boolean isTeamsEvent, int entryFee, String type, String formule) {
		
		setDate(Date);
		setMinAge(minAge);
		setMaxAge(maxAge);
		setWomenAllowed(womenAllowed);
		setMenAllowed(menAllowed);
		setTeamsEvent(isTeamsEvent);
		setEntryFee(entryFee);
		setType(type);
		setFormule(formule);
		this.Entries = new ArrayList<Fencer>();
		
		
		
	}
	
	public void addNewFencer(Fencer fencer) {
		
		if (Entries.contains(fencer)) throw new IllegalArgumentException("This fencer is already entered");
		
		Entries.add(fencer);
		
	}
	
	public void removeFencer(Fencer fencer) {
		if (Entries.contains(fencer)){
			Entries.remove(fencer);
		}else throw new IllegalArgumentException();
		
		
	}
	
	public boolean fencerIsEntered(Fencer fencer) {
		if (Entries.contains(fencer)) return true;
		
		return false;
	}
	

	public int getDate() {
		return Date;
	}

	public void setDate(int Date) {
		if (0 == Date) throw new IllegalArgumentException();
		
		this.Date = Date;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		if (0 == minAge) throw new IllegalArgumentException();
		
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		if (0 == maxAge) throw new IllegalArgumentException();
		
		this.maxAge = maxAge;
	}

	public boolean isWomenAllowed() {
		return womenAllowed;
	}

	public void setWomenAllowed(boolean womenAllowed) {
		this.womenAllowed = womenAllowed;
	}

	public boolean isMenAllowed() {
		return menAllowed;
	}

	public void setMenAllowed(boolean menAllowed) {
		this.menAllowed = menAllowed;
	}

	public boolean isTeamsEvent() {
		return isTeamsEvent;
	}

	public void setTeamsEvent(boolean isTeamsEvent) {
		this.isTeamsEvent = isTeamsEvent;
	}

	public int getEntryFee() {
		return entryFee;
	}

	public void setEntryFee(int entryFee) {
		if (entryFee < 0) throw new IllegalArgumentException();
		
		this.entryFee = entryFee;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if (null == type || "".equals(type.trim())) throw new IllegalArgumentException();
		
		this.type = type;
	}

	public String getFormule() {
		return formule;
	}

	public void setFormule(String formule) {
		if (null == formule || "".equals(formule.trim())) throw new IllegalArgumentException();
		
		this.formule = formule;
	}
	

	
	
	
	
}
