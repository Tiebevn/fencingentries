package domain;

import java.util.ArrayList;

public class Competition {

	String name;
	String location;
	Club organizer;
	ArrayList<Event> events;
	
	public Competition(String name, String location, Club organizer) {
	
		setName(name);
		setLocation(location);
		setOrganizer(organizer);
		this.events = new ArrayList<Event>();
		
	}
	
	public void addNewEvent(Event event) {
		if (isListedEvent(event)) throw new IllegalArgumentException();
		
		this.events.add(event);
	}
	
	public void removeEvent(Event event) {
		
		if (!isListedEvent(event)) throw new IllegalArgumentException();
		
		this.events.remove(event);
		
	}
	
	public boolean isListedEvent(Event event) {

		if(events.contains(event)) return true;
		
		return false;
		
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (null == name || "".equals(name.trim())) throw new IllegalArgumentException();
		
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if (null == location || "".equals(location.trim())) throw new IllegalArgumentException();
		
		this.location = location;
	}

	public Club getOrganizer() {
		return organizer;
	}

	public void setOrganizer(Club organizer) {
		if (null == organizer) throw new IllegalArgumentException();
		
		this.organizer = organizer;
	}

	public ArrayList<Event> getEvents() {
		return events;
	}


	
	
}
