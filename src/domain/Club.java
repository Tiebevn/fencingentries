package domain;



public class Club {

	String name;
	String location;
	Nation country;
	
	public Club(String name, String location, Nation country) {
		setName(name);
		setLocation(location);
		setCountry(country);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (null == name || "".equals(name.trim())) throw new IllegalArgumentException();
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if (null == location || "".equals(location.trim())) throw new IllegalArgumentException();
		this.location = location;
	}

	public Nation getCountry() {
		return country;
	}

	public void setCountry(Nation country) {
		
		if (null == country) throw new IllegalArgumentException();
		
		this.country = country;
	}
	
	
	
	
}
