package domain;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class ClubTest {

	private Club club;
	private Nation Belgium;
	
	@Before
	public void setUp() {
		Belgium = new Nation("Belgium");
		club = new Club("Parcival", "Leuven", Belgium);
	}
	
	@Test
	public void Valid_name_location_and_nation_create_new_club() {
		assertEquals("Parcival", club.getName());
		assertEquals("Leuven", club.getLocation());
		assertEquals(Belgium, club.getCountry());
	}
	
	@Test
	public void Add_a_valid_name_returns_a_valid_name() {
		club.setName("Testnaam");
		assertEquals("Testnaam", club.getName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Empty_name_throw_IllegalArgumentException() {
		club.setName(" ");
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void Null_name_throw_IllegalArgumentException() {
		club.setName(null);
	}
	
	
	@Test
	public void Add_a_valid_location_returns_a_valid_name() {
		club.setLocation("Testnaam");
		assertEquals("Testnaam", club.getLocation());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Null_location_throw_IllegalArgumentException() {
		club.setLocation(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Empty_location_throw_IllegalArgumentException() {
		club.setLocation(" ");
	}
	
	@Test
	public void Add_a_valid_name_returns_a_valid_nation() {
		club.setCountry(this.Belgium);
		assertEquals(this.Belgium, club.getCountry());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Null_country_throw_IllegalArgumentException() {
		club.setCountry(null);
	}
	
	

}
