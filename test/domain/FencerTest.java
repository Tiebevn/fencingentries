package domain;

import static org.junit.Assert.*;



import org.junit.Before;
import org.junit.Test;

public class FencerTest {
	
	private Club club;
	private Nation Belgium;
	private Fencer Fencer;
	
	@Before
	public void setUp() {
		this.Belgium = new Nation("Belgium");
		this.club = new Club("Parcival", "Leuven", Belgium);
		this.Fencer = new Fencer("Tiebe Van Nieuwenhove", 1997, 'm', 'r', this.club, 101098, this.Belgium);
	}

	@Test
	public void Add_fencer_with_all_valid_values_adds_new_fencer() {
		assertEquals("Tiebe Van Nieuwenhove", Fencer.getName());
		assertEquals(1997, Fencer.getBirthdate());
		assertEquals('m', Fencer.getSex());
		assertEquals('r', Fencer.getHanded());
		assertEquals(this.club, Fencer.getClub());
		assertEquals(101098, Fencer.getLicense());
		assertEquals(this.Belgium, Fencer.getNationality());
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_Empty_Name_throws_IllegalArgumentException() {
		Fencer.setName(" ");
	}
	
	@Test
	public void Add_valid_name_to_fencer() {
		Fencer.setName("Testnaam");
		
		assertEquals("Testnaam", Fencer.getName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_Null_Name_throws_IllegalArgumentException() {
		Fencer.setName(null);
	}
	
	@Test
	public void Add_valid_birthdate_to_fencer() {
		Fencer.setBirthdate(1998);
		
		assertEquals(1998, Fencer.getBirthdate());
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_invalid_Birthdate_throws_IllegalArgumentException() {
		Fencer.setBirthdate(-352);
	}
	
	@Test
	public void Add_valid_sex_to_fencer() {
		Fencer.setSex('f');
		
		assertEquals('f', Fencer.getSex());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_invalid_Sex_throws_IllegalArgumentException() {
		Fencer.setSex('v');
	}
	
	
	@Test
	public void Add_valid_hand_to_fencer() {
		Fencer.setHanded('l');
		
		assertEquals('l', Fencer.getHanded());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_invalid_hand_throws_IllegalArgumentException() {
		Fencer.setHanded('a');
	}
	
	@Test
	public void Add_valid_club_to_fencer() {
		Fencer.setClub(this.club);
		
		assertEquals(this.club, Fencer.getClub());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_empty_club_throws_IllegalArgumentException() {
		Fencer.setClub(null);
	}
	
	@Test
	public void Add_valid_license_to_fencer() {
		Fencer.setLicense(1998);
		
		assertEquals(1998, Fencer.getLicense());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_invalid_license_throws_IllegalArgumentException() {
		Fencer.setLicense(-352);
	}
	
	@Test
	public void Add_valid_nationality_to_fencer() {
		Fencer.setNationality(this.Belgium);
		
		assertEquals(this.Belgium, Fencer.getNationality());
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void Add_Fencer_With_empty_Nationality_throws_IllegalArgumentException() {
		Fencer.setNationality(null);
	}

}
