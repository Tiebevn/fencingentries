package domain;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

public class NationTest {

	private Nation nationValid, nationValidNoCode;
	
	@Before
	public void setUp() {
		nationValid = new Nation("Belgium", "BEL");
		nationValidNoCode = new Nation("Belgium");
		
		
	}
	
	@Test
	public void Nation_with_valid_name_and_code_return_valid_nation() {
		assertEquals("Belgium", nationValid.getName());
		assertEquals("BEL", nationValid.getCode());
		
	}
	
	@Test
	public void Nation_with_valid_name_without_code_return_valid_nation() {
		assertEquals("Belgium", nationValidNoCode.getName());
		assertEquals("BEL", nationValid.getCode());
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Nation_with_empty_name_and_code_throws_IllegalArgumentException() {
		nationValid.setName("");
		
	}
	
	
	
	

}
