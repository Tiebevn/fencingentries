package domain;

import static org.junit.Assert.*;




import org.junit.Before;
import org.junit.Test;

public class EventTest {

	
	private Club club;
	private Nation Belgium;
	private Fencer Fencer;
	private Event event;
	

	
	
	@Before
	public void setUp() {
		this.Belgium = new Nation("Belgium");
		this.club = new Club("Parcival", "Leuven", Belgium);
		this.Fencer = new Fencer("Tiebe Van Nieuwenhove", 1997, 'm', 'r', this.club, 101098, this.Belgium);
		this.event = new Event(2017, 1999, 1997, true, true, false, 20, "Nationaal Circuit", "2 rounds of pools and direct elimination");
		
	}
	
	
	@Test
	public void Constructor_with_all_valid_values_makes_new_event() {
		assertEquals(2017, event.getDate());
		assertEquals(1997, event.getMaxAge());
		assertEquals(1999, event.getMinAge());
		assertEquals(true, event.isWomenAllowed());
		assertEquals(true, event.isMenAllowed());
		assertEquals(false, event.isTeamsEvent());
		assertEquals(20, event.getEntryFee());
		assertEquals("Nationaal Circuit", event.getType());
		assertEquals("2 rounds of pools and direct elimination", event.getFormule());
		
	}
	
	@Test
	public void Add_a_new_unique_fencer_adds_fencer_to_Entries() {
		event.addNewFencer(this.Fencer);
		assertTrue(event.fencerIsEntered(Fencer));	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Adding_a_duplicate_fencer_throws_IllegalArgumentException() {
		event.addNewFencer(this.Fencer);
		event.addNewFencer(this.Fencer);	
	}
	
	@Test
	public void Remove_an_existing_fencer_removes_fencer_from_list() {
		event.addNewFencer(this.Fencer);
		
		assertTrue(event.fencerIsEntered(Fencer));
		
		event.removeFencer(Fencer);
		
		assertFalse(event.fencerIsEntered(Fencer));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Remove_non_existing_fencer_from_list_throws_new_IllegalArgumentException() {
		event.removeFencer(Fencer);
	}
	
	@Test
	public void Find_existing_fencer_returns_true() {
		event.addNewFencer(Fencer);
		
		assertTrue(event.fencerIsEntered(Fencer));
	}
	
	@Test
	public void Find_nonexisting_fencer_returns_false() {
		
		assertFalse(event.fencerIsEntered(Fencer));
	}
	
	@Test
	public void Change_to_different_valid_entry_fee() {
		event.setEntryFee(30);
		assertEquals(30, event.getEntryFee());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Change_to_invalid_entry_fee() {
		event.setEntryFee(-20);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Change_formule_to_null_string_throws_IllegalArgumentException(){
		event.setFormule(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Change_formule_to_empty_string_throws_IllegalArgumentException(){
		event.setFormule(" ");
	}
	
	@Test
	public void Change_Formule_to_valid_string(){
		event.setFormule("Test");
		assertEquals("Test", event.getFormule());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Change_Type_to_null_string_throws_IllegalArgumentException(){
		event.setType(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Change_Type_to_empty_string_throws_IllegalArgumentException(){
		event.setType(" ");
	}
	
	@Test
	public void Change_Type_to_valid_string(){
		event.setType("Test");
		assertEquals("Test", event.getType());
	}
	
	
	

}
