package domain;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

public class CompetitionTest {

	
	private Club club;
	private Nation Belgium;
	private Fencer Fencer;
	private Event event;
	private Competition competition;
	

	
	
	@Before
	public void setUp() {
		this.Belgium = new Nation("Belgium");
		this.club = new Club("Parcival", "Leuven", Belgium);
		this.Fencer = new Fencer("Tiebe Van Nieuwenhove", 1997, 'm', 'r', this.club, 101098, this.Belgium);
		this.event = new Event(2017, 1999, 1997, true, true, false, 20, "Nationaal Circuit", "2 rounds of pools and direct elimination");
		this.competition = new Competition("GPSL", "Leuven", club);
	}
	
	@Test
	public void Constructor_with_valid_values_creates_new_competition() {
		assertEquals("GPSL", competition.getName());
		assertEquals("Leuven", competition.getLocation());
		assertEquals(this.club, competition.getOrganizer());
	}
	
	@Test
	public void Adding_a_new_unique_event_adds_event_to_list() {
		competition.addNewEvent(event);
		assertTrue(competition.isListedEvent(event));
	}
	
	@Test 
	public void Removing_an_existing_event_removes_the_event() {
		
		competition.addNewEvent(event);
		assertTrue(competition.isListedEvent(event));
		competition.removeEvent(event);
		assertFalse(competition.isListedEvent(event));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Adding_an_already_existing_event_throws_IllegalArgumentException() {
		competition.addNewEvent(event);
		competition.addNewEvent(event);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Removing_a_non_existing_event_throws_IllegalArgumentException() {
		competition.removeEvent(event);
	}
	
	@Test
	public void Changing_tournament_name_to_another_valid_value_changes_name() {
		competition.setName("Testname");
		assertEquals("Testname", competition.getName());
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Changing_tournament_name_to_null_name_throws_IllegalArgumentException() {
		competition.setName(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Changing_tournament_name_to_empty_name_throws_IllegalArgumentException() {
		competition.setName(" ");
	}
	
	@Test
	public void Changing_tournament_location_to_another_valid_value_changes_location() {
		competition.setLocation("TestLocation");
		assertEquals("TestLocation", competition.getLocation());
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Changing_tournament_location_to_null_location_throws_IllegalArgumentException() {
		competition.setLocation(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Changing_tournament_location_to_empty_location_throws_IllegalArgumentException() {
		competition.setLocation(" ");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Setting_a_null_organizer_throws_new_IllegalArgumentException() {
		competition.setOrganizer(null);
	}
	
	@Test
	public void Changing_organizer_changes_organizer_to_valid_club() {
		competition.setOrganizer(club);
		assertEquals(club, competition.getOrganizer());
	}
	
	

}
